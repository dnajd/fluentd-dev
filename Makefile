

.DEFAULT_GOAL := help
BLUE := $(shell tput setaf 4)
RESET := $(shell tput sgr0)

help : ## fluentd dev
	@grep -E '^[^ .]+: .*?## .*$$' Makefile \
        | awk '\
            BEGIN { FS = ": .*##" };\
            { printf "%-20s$(RESET) %s\n", $$1, $$2  }'

init: ## init the app
	docker-compose down --volumes
	docker-compose build

up: ## up the worker
	docker-compose up -d

down: ## down the worker
	docker-compose down --volumes

bash: ## ssh into worker bash
	docker-compose run \
		-e FLUENTD_CONF=fluent.conf \
		-e TEST=true \
		--rm worker bash

tests: ## run the tests
	docker-compose up -d
	docker-compose restart worker
	docker-compose run --rm worker rake test

logs: ## see the logs
	docker-compose logs worker

mlem: ## who knows?
	curl -i -X POST -d 'json={"message":"hi1"}' http://0.0.0.0:9880/test.cycle/
	curl -i -X POST -d 'json={"message":"hi3", "action": "skip"}' http://0.0.0.0:9880/test.cycle/

clean: ## clean rendered fluentd confs
	rm fluentd/.rendered/*.conf