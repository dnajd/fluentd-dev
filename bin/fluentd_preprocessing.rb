require 'erb'
require 'fileutils'

Dir.foreach('fluentd') do |source_filename|
  next if source_filename == '.' or source_filename == '..'
  source_filepath = File.join('fluentd', source_filename)
  target_filepath = File.join('/app/fluentd/.rendered', File.basename(source_filename))

  if source_filename.end_with? '.conf'
    puts "rendering (#{source_filepath}) to #{target_filepath})"

    # render erb
    erb_source = File.read(source_filepath)
    renderer = ERB.new(erb_source)

    # save file
    File.write(target_filepath, renderer.result())
  end
end

