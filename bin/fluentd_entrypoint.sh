#!/bin/sh

# proprocess
ruby /app/bin/fluentd_preprocessing.rb

# If the user has supplied only arguments append them to `fluentd` command
if [ "${1#-}" != "$1" ]; then
    set -- fluentd "$@"
fi

# If user does not supply config file or plugins, lets set them
if [ "$1" = "fluentd" ]; then

    # config file
    if ! echo $@ | grep ' \-c' ; then
       set -- "$@" -c /app/fluentd/.rendered/${FLUENTD_CONF}
    fi

    # plugins folder
    if ! echo $@ | grep ' \-p' ; then
       set -- "$@" -p /app/fluentd/plugins
    fi
fi

exec "$@"