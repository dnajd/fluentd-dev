require 'fluent/plugin/input'

module Fluent
  module Plugin
    class ExamplePluginInput < Input
      # For `@type my_awesome` in configuration file
      Fluent::Plugin.register_input('example_plugin', self)

      def configure(conf)
        super
      end

      def start
        super
        # ...
      end
    end
  end
end