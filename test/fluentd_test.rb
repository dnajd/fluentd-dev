require 'json'
require 'minitest/autorun'


def emit_log(tag, log)
  formatted = JSON.generate(log).gsub("'", "\\\\'")
  `echo '#{formatted}' | fluent-cat -h 0.0.0.0 #{tag}`
end

class TestMeme < MiniTest::Unit::TestCase
  def setup

  end

  def test_emit_log
    emit_log('test.packages', {'item':1})
    assert true
  end
end
