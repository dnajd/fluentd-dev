FROM debian

RUN apt-get update -qq && apt-get install -y \
    net-tools \
    build-essential \
    ruby-full \
    libmariadb-dev \
;

RUN gem install --no-document \
    curl \
    fluentd \
    mysql2 \
    rake \
;

RUN fluent-gem install --no-document \
    fluent-plugin-elasticsearch \
    fluent-plugin-record_splitter \
    fluent-plugin-sql \
    fluent-plugin-mysql-fetch-and-emit \
    fluent-plugin-mysql-replicator \
;

WORKDIR /app
COPY . /app

ENTRYPOINT ["/app/bin/fluentd_entrypoint.sh"]
CMD ["fluentd"]
