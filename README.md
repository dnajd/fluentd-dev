# Fluentd Dev

Test driven development with Fluentd. How will we do that?

Inspired by [how-to-test-fluentd-config](https://knplabs.com/en/blog/how2tips-how-to-test-fluentd-config)

# Just use it

Use make to get going...

`make init` - to get started
`make up` - to bring everything up

run `make` to see all the commands you can use

```
$ make

init                  init the app
up                    up the worker
down                  down the worker
bash                  ssh into worker bash
tests                 run the tests
logs                  see the logs
mlem                  who knows?
clean                 clean rendered fluentd confs
```

# Dockerized

The `Dockerfile` uses a `bin/fluentd_entrypoint.sh` that...

* runs `bin/fluentd_preprocessor.rb` which lets us use ERB ruby in our fluentd conf files
* it also sets defaults for the fluent config and plugin folders

When you launch the container it
* expects FLUENTD_CONF=filename.conf pointing to the configuration file you want to run
* it performs the configuration preprocessing and moves the files into `fluentd/.rendered` where you can look at them. These are gitignored.
* it launches fluentd

# Fluentd Configuration Files

Create all your fluentd config files in the folder `fluentd/`.  You'll see a few other folders in there:

* .rendered - are the configuration files after they've been rendered through ERB; these are gitignored.
* examples - these are for inspiration and example
* partials - create files here and reuse them in your different fluentd config files
* plugins - write your own fluentd plugins here

## Partials

Create partials in `fluentd/partials` and start the filename with an "_".  Then in your fluentd config you can include them like so:

```
   <%= File.open('fluentd/partials/_test_source.conf').read %>
```

## Testing

Run the tests with `make tests`.  Here are the basic pieces that make testing work.

1. You use ruby to swap out the source in test mode

```
<% if ENV['TEST'] %>
    <source>
    @type forward
    port 24224
    bind 0.0.0.0
    </source>
<% else %>
    <source>
    @type elasticsearch
    host elasticsearch
    port 9200
    index_name packages-name
    type_name _doc
    tag test
    </source>
<% end %>
```

Now the tests can emit data to the fluentd worker over the network

```
    tag = 'test.data'
    data = {first: 'bob', last: 'jones'}
    `echo '#{data.to_json}' | fluent-cat #{tag}`
```

You can do the same thing for output store

```
<% if ENV['TEST'] %>
   <store>
      @type file
      path /tmp/example_testable.log
      append true
      add_path_suffix false
      <format>
         @type json
      </format>
      <inject>
         tag_key tag
         time_key date
         time_type string
         time_format %F:%T
      </inject>
      <buffer []>
         @type memory
         chunk_limit_records 1
         retry_max_times 0
      </buffer>
   </store>
<% else %>
   
<% end %>
```

Then use the test helper to asset the record made it into the logfile. TBD.
